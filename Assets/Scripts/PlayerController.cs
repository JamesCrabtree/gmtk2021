﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private const float speed = 15;
    //private const float maxSpeed = 10;
    //private const int groundMask = 1 << 8;

    private Rigidbody2D rb;
    private IsNearPlatform nearPlatformScript;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        nearPlatformScript = GetComponentInChildren<IsNearPlatform>();
    }

    void FixedUpdate()
    {
        if (canMove())
        {
            float h = Input.GetAxis("Horizontal");
            //float horizontalVelocity = Mathf.Min(rb.velocity.x + (h * speed * Time.fixedDeltaTime), maxSpeed);
            rb.velocity = new Vector2(h * speed, rb.velocity.y);
        }
    }

    private bool canMove()
    {
        return nearPlatformScript != null && rb != null && nearPlatformScript.NearPlatform;
    }
}
