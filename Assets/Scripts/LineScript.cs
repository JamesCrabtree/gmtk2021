﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineScript : MonoBehaviour
{
    public Transform blueCharacter, redCharacter;

    LineRenderer lineRenderer;

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = 2;
    }

    void LateUpdate()
    {
        if (blueCharacter == null || blueCharacter.gameObject == null || redCharacter == null || redCharacter.gameObject == null || lineRenderer == null)
            return;

        Vector3[] positions = new Vector3[2];
        positions[0] = blueCharacter.position;
        positions[1] = redCharacter.position;
        lineRenderer.SetPositions(positions);
    }
}
