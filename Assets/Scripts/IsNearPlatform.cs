﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsNearPlatform : MonoBehaviour
{
    private bool isNearPlatform;

    public bool NearPlatform
    {
        get
        {
            return isNearPlatform;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Ground"))
            isNearPlatform = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Ground"))
            isNearPlatform = false;
    }
}
